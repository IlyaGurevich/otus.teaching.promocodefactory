﻿namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class RoleRequest
    {
        public string RoleName { get; set; }
        public string RoleDescription { get; set; }
    }
}
