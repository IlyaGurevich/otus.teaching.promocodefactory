﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class EmployeeRequestUpdate
    {
            public Guid Id { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }

            public string Email { get; set; }

            public List<RoleRequest> Roles { get; set; }

            public int AppliedPromocodesCount { get; set; }
    }
}
