﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public UserController(IConfiguration configuration) 
        {
            var username = configuration["UserParam"];
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(int id) 
        {
            return Ok(new UserModel());
        }

        [HttpPost]
        public IActionResult AddUser(UserModel userModel)
        {
            return Ok(1);
        }
    }
}
