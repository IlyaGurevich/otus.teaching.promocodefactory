﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }

        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddAsync(T entity)
        {
            (Data as List<T>).Add(entity);
            return Task.CompletedTask;
        }

        public Task UpdateByIdAsync(T entity)
        {
            var updatedEmployee = (Data as List<T>).Where(x => x.Id == entity.Id);
            (Data as List<T>)[(Data as List<T>).IndexOf(updatedEmployee.FirstOrDefault())] = entity;
            return Task.CompletedTask;
        }

        public Task DeleteByIdAsync(Guid id) 
        {
            var deletedEmployee = (Data as List<T>).Where(x => x.Id == id);
            (Data as List<T>).Remove(deletedEmployee.FirstOrDefault());
            return Task.CompletedTask;
        }

        public Task DeleteAllAsync() 
        {
            (Data as List<T>).Clear();
            return Task.CompletedTask;
        }
    }
}